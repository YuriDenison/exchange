package com.revolut.exchange.mock

import android.app.Application
import com.jakewharton.rxrelay2.BehaviorRelay
import com.revolut.exchange.model.Currency.EUR
import com.revolut.exchange.model.Currency.GBP
import com.revolut.exchange.model.Currency.USD
import com.revolut.exchange.model.Rate
import com.revolut.exchange.model.RatesModel
import com.revolut.exchange.network.FixerApiService
import com.revolut.exchange.repository.RatesRepository
import io.reactivex.Observable

class MockRatesRepository(application: Application, api: FixerApiService) : RatesRepository(application, api) {
  val ratesStream: BehaviorRelay<RatesModel> = BehaviorRelay.create<RatesModel>().apply {
    accept(RatesModel.Data(setOf(
        Rate(USD, EUR, 2.0),
        Rate(EUR, USD, 0.5),
        Rate(USD, GBP, 5.0),
        Rate(GBP, USD, 0.2),
        Rate(GBP, USD, 2.5),
        Rate(USD, GBP, 0.4),
        Rate(USD, EUR, 1.0),
        Rate(EUR, EUR, 1.0),
        Rate(GBP, GBP, 1.0)
    )))
  }

  override fun observeRates(): Observable<RatesModel> = ratesStream
}