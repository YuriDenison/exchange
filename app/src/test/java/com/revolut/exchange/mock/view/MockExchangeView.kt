package com.revolut.exchange.mock.view

import com.jakewharton.rxrelay2.PublishRelay
import com.revolut.exchange.ui.exchange.ExchangeView
import com.revolut.exchange.ui.exchange.ExchangeView.ExchangeViewModel
import com.revolut.exchange.ui.exchange.ExchangeView.ViewAction
import io.reactivex.Observable

open class MockExchangeView : ExchangeView {
  val actionStream: PublishRelay<ViewAction> = PublishRelay.create<ViewAction>()

  override fun bindTo(model: ExchangeViewModel) {}
  override fun observeViewActions(): Observable<ViewAction> = actionStream
}