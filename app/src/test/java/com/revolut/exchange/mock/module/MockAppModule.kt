package com.revolut.exchange.mock.module

import android.app.Application
import com.revolut.exchange.ExchangeApplication
import com.revolut.exchange.dependency.module.AppModule
import com.revolut.exchange.mock.MockAppPreferences
import com.revolut.exchange.preferences.AppPreferences

class MockAppModule(app: ExchangeApplication) : AppModule(app) {
  override fun provideAppPreferences(app: Application): AppPreferences = MockAppPreferences()
}