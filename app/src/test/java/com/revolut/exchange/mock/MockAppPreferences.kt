package com.revolut.exchange.mock

import com.revolut.exchange.model.Account
import com.revolut.exchange.model.Currency.EUR
import com.revolut.exchange.model.Currency.GBP
import com.revolut.exchange.model.Currency.USD
import com.revolut.exchange.preferences.AppPreferences
import com.revolut.exchange.preferences.data.common.BoolValue
import com.revolut.exchange.preferences.data.custom.AccountValue

class MockAppPreferences(
    private val defaultAccount: Account = createDefaultAccount(),
    private val defaultWelcomeMessageShown: Boolean = true
) : AppPreferences {

  private val delegate = NoOpKeyValueDelegate()

  override fun accountValue() = AccountValue(delegate, "", defaultAccount)
  override fun welcomeMessageShown() = BoolValue(delegate, "", defaultWelcomeMessageShown)

  private companion object {
    fun createDefaultAccount() = Account(mapOf(
        USD to 1000.0,
        EUR to 0.0,
        GBP to 0.0
    ))
  }
}