package com.revolut.exchange.mock.module

import android.app.Application
import com.revolut.exchange.dependency.module.RepositoryModule
import com.revolut.exchange.mock.MockRatesRepository
import com.revolut.exchange.network.FixerApiService
import com.revolut.exchange.repository.RatesRepository

class MockRepositoryModule : RepositoryModule() {
  override fun provideRatesRepository(app: Application, api: FixerApiService): RatesRepository = MockRatesRepository(app, api)
}