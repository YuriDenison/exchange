package com.revolut.exchange.mock

import com.revolut.exchange.preferences.delegate.KeyValueDelegate
import io.reactivex.Observable

class NoOpKeyValueDelegate : KeyValueDelegate {
  private val map = mutableMapOf<String, String>()

  override fun getInt(key: String, defaultValue: Int) = defaultValue

  override fun putInt(key: String, value: Int) {}

  override fun getBoolean(key: String, defaultValue: Boolean) = defaultValue

  override fun putBoolean(key: String, value: Boolean) {}

  override fun getLong(key: String, defaultValue: Long) = defaultValue

  override fun putLong(key: String, value: Long) {}

  override fun getString(key: String, defaultValue: String) = map[key] ?: defaultValue

  override fun putString(key: String, value: String) {
    map.put(key, value)
  }

  override fun getDouble(key: String, defaultValue: Double) = defaultValue

  override fun putDouble(key: String, value: Double) {}

  override fun getFloat(key: String, defaultValue: Float) = defaultValue

  override fun putFloat(key: String, value: Float) {}

  override fun getStringSet(key: String): Set<String> = emptySet()

  override fun putStringSet(key: String, value: Set<String>) {}

  override fun contains(key: String) = false

  override fun remove(key: String) {}

  override fun clear() {}

  override fun keyChanges(): Observable<String> = Observable.just("")
}