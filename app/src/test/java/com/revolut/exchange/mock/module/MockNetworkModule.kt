package com.revolut.exchange.mock.module

import com.revolut.exchange.dependency.module.NetworkModule
import com.revolut.exchange.model.Currency
import com.revolut.exchange.model.RatesModel
import com.revolut.exchange.network.FixerApi
import com.revolut.exchange.network.FixerApiService
import dagger.Provides
import io.reactivex.Single
import javax.inject.Singleton

class MockNetworkModule : NetworkModule() {
  @Provides
  @Singleton
  override fun provideFixerApiService(api: FixerApi, currencies: Currency.Container): FixerApiService =
      object : FixerApiService(api, currencies.items) {
        override fun latestRates(): Single<RatesModel> = Single.never()
      }
}