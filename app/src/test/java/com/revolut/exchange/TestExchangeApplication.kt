package com.revolut.exchange

import com.revolut.exchange.dependency.AppComponent
import com.revolut.exchange.dependency.DaggerAppComponent
import com.revolut.exchange.mock.module.MockAppModule
import com.revolut.exchange.mock.module.MockNetworkModule
import com.revolut.exchange.mock.module.MockRepositoryModule

class TestExchangeApplication : ExchangeApplication() {
  override fun createAppComponent(): AppComponent {
    return DaggerAppComponent.builder()
        .appModule(MockAppModule(this))
        .networkModule(MockNetworkModule())
        .repositoryModule(MockRepositoryModule())
        .build()
  }
}