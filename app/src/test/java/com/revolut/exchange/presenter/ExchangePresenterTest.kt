package com.revolut.exchange.presenter

import com.revolut.exchange.BuildConfig
import com.revolut.exchange.ExchangeApplication
import com.revolut.exchange.RxJavaTestRunner
import com.revolut.exchange.TestExchangeApplication
import com.revolut.exchange.mock.view.MockExchangeView
import com.revolut.exchange.model.Currency.EUR
import com.revolut.exchange.model.Currency.USD
import com.revolut.exchange.preferences.AppPreferences
import com.revolut.exchange.ui.exchange.DaggerExchangeComponent
import com.revolut.exchange.ui.exchange.ExchangePresenter
import com.revolut.exchange.ui.exchange.ExchangeView.ViewAction
import com.revolut.exchange.ui.exchange.currency.CurrencyItem
import com.revolut.exchange.ui.exchange.currency.CurrencyItem.Side.SOURCE
import com.revolut.exchange.ui.exchange.currency.CurrencyItem.Side.TARGET
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.mockito.Spy
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

/**
 * Just small proof-of-concept test
 */
@RunWith(RxJavaTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(26), application = TestExchangeApplication::class)
class ExchangePresenterTest {
  @Spy private lateinit var view: MockExchangeView
  private lateinit var appPreferences: AppPreferences

  private lateinit var presenter: ExchangePresenter

  @Before
  fun setUp() {
    view = MockExchangeView()
    appPreferences = appComponent().preferences()

    MockitoAnnotations.initMocks(this)

    presenter = DaggerExchangeComponent.builder()
        .appComponent(appComponent())
        .build()
        .presenter()
    presenter.attachView(view)
  }

  @Test
  @Throws(Exception::class)
  fun testTransaction() {
    val portfolio = appPreferences.accountValue().get().portfolio
    assertEquals(portfolio[USD], 1000.0)
    assertEquals(portfolio[EUR], 0.0)

    view.actionStream.accept(ViewAction.CurrencyUpdateAction(CurrencyItem.Model(TARGET, EUR, 150.0, 0.0, false, true)))
    view.actionStream.accept(ViewAction.CurrencyUpdateAction(CurrencyItem.Model(SOURCE, USD, 150.0, 100.0, true, true)))
    view.actionStream.accept(ViewAction.SubmitClicked)

    val updatedPortfolio = appPreferences.accountValue().get().portfolio
    assertEquals(updatedPortfolio[USD], 900.0)
    assertEquals(updatedPortfolio[EUR], 200.0)
  }

  @After
  fun after() {
    presenter.detachView()
  }

  private companion object {
    fun appComponent() = ExchangeApplication.component(RuntimeEnvironment.application)
  }
}