package com.revolut.exchange

import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.robolectric.RobolectricTestRunner

class RxJavaTestRunner(testClass: Class<*>) : RobolectricTestRunner(testClass) {
  init {
    RxJavaPlugins.reset()
    RxJavaPlugins.onIoScheduler(Schedulers.trampoline())
    RxAndroidPlugins.onMainThreadScheduler(Schedulers.trampoline())
  }
}