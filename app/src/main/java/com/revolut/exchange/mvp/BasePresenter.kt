package com.revolut.exchange.mvp

import android.support.annotation.CallSuper
import android.support.annotation.UiThread
import com.revolut.exchange.util.CompositeDisposableComponent
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.lang.ref.WeakReference

open class BasePresenter<V : MvpView> : MvpPresenter<V>, CompositeDisposableComponent by CompositeDisposableComponent.Impl() {
  private lateinit var viewRef: WeakReference<V>

  protected val view: V?
    get() = viewRef.get()

  @CallSuper
  @UiThread
  override fun attachView(view: V) {
    viewRef = WeakReference(view)
  }

  @CallSuper
  @UiThread
  override fun detachView() {
    resetCompositeDisposable()
    viewRef.clear()
  }

  protected fun <T : Any> Observable<T>.subscribeUntilDetach(
      scheduler: Scheduler = Schedulers.io(),
      onNext: (T) -> Unit = onNextStub,
      onError: (Throwable) -> Unit = onErrorStub,
      onComplete: () -> Unit = onCompleteStub
  ) = subscribeOn(scheduler)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(onNext, onError, onComplete)
      .keepUntilDetach()

  protected fun Completable.subscribeUntilDetach(
      scheduler: Scheduler = Schedulers.io(),
      onSuccess: () -> Unit = {},
      onError: (Throwable) -> Unit = onErrorStub
  ) = subscribeOn(scheduler)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(onSuccess, onError)
      .keepUntilDetach()

  protected fun <T : Any> Single<T>.subscribeUntilDetach(
      scheduler: Scheduler = Schedulers.io(),
      onSuccess: (T) -> Unit = onNextStub,
      onError: (Throwable) -> Unit = onErrorStub
  ) = subscribeOn(scheduler)
      .observeOn(AndroidSchedulers.mainThread())
      .subscribe(onSuccess, onError)
      .keepUntilDetach()

  private companion object {
    private val onNextStub: (Any) -> Unit = {}
    private val onErrorStub: (Throwable) -> Unit = { Timber.e(it, "On error not implemented") }
    private val onCompleteStub: () -> Unit = {}
  }
}