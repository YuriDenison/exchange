package com.revolut.exchange.mvp

import android.support.annotation.UiThread

interface MvpPresenter<in V : MvpView> {
  @UiThread fun attachView(view: V)
  @UiThread fun detachView()
}