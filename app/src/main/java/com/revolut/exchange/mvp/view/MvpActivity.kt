package com.revolut.exchange.mvp.view

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import com.revolut.exchange.ExchangeApplication
import com.revolut.exchange.mvp.MvpPresenter
import com.revolut.exchange.mvp.MvpView
import com.revolut.exchange.util.CompositeDisposableComponent

abstract class MvpActivity<V : MvpView, out P : MvpPresenter<V>, S : Any> : AppCompatActivity(), MvpView,
    CompositeDisposableComponent by CompositeDisposableComponent.Impl() {
  private val presenter: P by lazy { createPresenter() }

  @CallSuper
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    appComponent().router().onActivityCreated(this)
    setContentView(getLayoutId())

    lastCustomNonConfigurationInstance?.let {
      @Suppress("UNCHECKED_CAST")
      restoreFromSnapshot(it as S)
    } ?: injectDependencies()

    presenter.attachView(getMvpView())
  }

  override fun onDestroy() {
    presenter.detachView()
    resetCompositeDisposable()
    appComponent().router().onActivityDestroyed()
    super.onDestroy()
  }

  override fun onRetainCustomNonConfigurationInstance() = takeSnapshot()

  protected open fun restoreFromSnapshot(snapshot: S) {}

  protected open fun takeSnapshot(): S? = null

  @Suppress("UNCHECKED_CAST")
  protected open fun getMvpView() = this as V

  protected open fun injectDependencies() {}

  protected fun appComponent() = ExchangeApplication.component(this)

  @LayoutRes
  abstract fun getLayoutId(): Int

  abstract fun createPresenter(): P
}