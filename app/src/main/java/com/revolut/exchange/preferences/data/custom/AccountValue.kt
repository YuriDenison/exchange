package com.revolut.exchange.preferences.data.custom

import com.google.gson.Gson
import com.revolut.exchange.model.Account
import com.revolut.exchange.model.Currency
import com.revolut.exchange.preferences.data.TypedValue
import com.revolut.exchange.preferences.delegate.KeyValueDelegate

class AccountValue(delegate: KeyValueDelegate, key: String, defaultAccount: Account) : TypedValue<Account>(delegate, key, defaultAccount) {
  override fun get(): Account = gson.fromJson(delegate.getString(key, ""), Account::class.java) ?: defaultValue

  override fun set(value: Account) {
    delegate.putString(key, gson.toJson(value, Account::class.java))
  }

  fun updateWith(source: Pair<Currency, Double>, target: Pair<Currency, Double>) {
    val value = get()
    val newValue = Account(
        value.portfolio.toMutableMap().apply {
          put(source.first, getOrDefault(source.first, 0.0) + source.second)
          put(target.first, getOrDefault(target.first, 0.0) + target.second)
        }
    )
    set(newValue)
  }

  private companion object {
    val gson = Gson()
  }
}