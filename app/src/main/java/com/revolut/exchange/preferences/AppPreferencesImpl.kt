package com.revolut.exchange.preferences

import android.app.Application
import android.preference.PreferenceManager
import com.revolut.exchange.model.Account
import com.revolut.exchange.model.Currency
import com.revolut.exchange.preferences.data.common.BoolValue
import com.revolut.exchange.preferences.data.custom.AccountValue
import com.revolut.exchange.preferences.delegate.PreferenceDelegate

class AppPreferencesImpl(app: Application) : AppPreferences {
  private val delegate = PreferenceDelegate(PreferenceManager.getDefaultSharedPreferences(app))

  override fun accountValue() = AccountValue(delegate, KEY_ACCOUNT, createDefaultAccount())
  override fun welcomeMessageShown() = BoolValue(delegate, KEY_FIRST_LAUNCH)

  private companion object {
    const val KEY_ACCOUNT = ".KEY_ACCOUNT"
    const val KEY_FIRST_LAUNCH = ".KEY_FIRST_LAUNCH"

    fun createDefaultAccount() = Account(mapOf(
        Currency.USD to 1000.0,
        Currency.EUR to 0.0,
        Currency.GBP to 0.0
    ))
  }
}