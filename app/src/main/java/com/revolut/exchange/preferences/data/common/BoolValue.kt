package com.revolut.exchange.preferences.data.common

import com.revolut.exchange.preferences.data.TypedValue
import com.revolut.exchange.preferences.delegate.KeyValueDelegate

class BoolValue @JvmOverloads constructor(delegate: KeyValueDelegate, key: String, defaultValue: Boolean = EMPTY)
  : TypedValue<Boolean>(delegate, key, defaultValue) {

  override fun get() = delegate.getBoolean(key, defaultValue)

  override fun set(value: Boolean) = delegate.putBoolean(key, value)

  fun checkFlagWithAction(action: () -> Unit) {
    if (get().not())
      action()
    set(true)
  }

  companion object {
    const val EMPTY = false
  }
}
