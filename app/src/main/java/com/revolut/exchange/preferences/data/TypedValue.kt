package com.revolut.exchange.preferences.data

import com.revolut.exchange.preferences.delegate.KeyValueDelegate
import io.reactivex.Observable

abstract class TypedValue<T> protected constructor(protected val delegate: KeyValueDelegate, val key: String, protected val defaultValue: T) {

  abstract fun get(): T

  abstract fun set(value: T)

  fun remove() {
    delegate.remove(key)
  }

  open fun isSet() = delegate.contains(key)

  fun asObservable(): Observable<T> = delegate.keyChanges()
      .filter { it == key }
      .map({ get() })
}
