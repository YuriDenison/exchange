package com.revolut.exchange.preferences

import com.revolut.exchange.preferences.data.common.BoolValue
import com.revolut.exchange.preferences.data.custom.AccountValue

interface AppPreferences {
  fun accountValue(): AccountValue
  fun welcomeMessageShown(): BoolValue
}