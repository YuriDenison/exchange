package com.revolut.exchange.preferences.delegate

import io.reactivex.Observable

interface KeyValueDelegate {

  fun getInt(key: String, defaultValue: Int): Int

  fun putInt(key: String, value: Int)

  fun getBoolean(key: String, defaultValue: Boolean): Boolean

  fun putBoolean(key: String, value: Boolean)

  fun getLong(key: String, defaultValue: Long): Long

  fun putLong(key: String, value: Long)

  fun getString(key: String, defaultValue: String): String

  fun putString(key: String, value: String)

  fun getDouble(key: String, defaultValue: Double): Double

  fun putDouble(key: String, value: Double)

  fun getFloat(key: String, defaultValue: Float): Float

  fun putFloat(key: String, value: Float)

  fun getStringSet(key: String): Set<String>

  fun putStringSet(key: String, value: Set<String>)

  fun contains(key: String): Boolean

  fun remove(key: String)

  fun clear()

  fun keyChanges(): Observable<String>
}
