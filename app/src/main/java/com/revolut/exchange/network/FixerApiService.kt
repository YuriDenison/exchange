package com.revolut.exchange.network

import com.revolut.exchange.model.Currency
import com.revolut.exchange.model.Rate
import com.revolut.exchange.model.RatesModel
import com.revolut.exchange.network.data.ResponseData.Rates
import com.revolut.exchange.util.zipWith
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

open class FixerApiService(private val api: FixerApi, private val supportedCurrencies: List<Currency>) {
  open fun latestRates(): Single<RatesModel> =
      supportedCurrencies.map { requestRates(it) }.zipWith { mergeRates(it) }
          .map { RatesModel.Data(it) as RatesModel }
          .onErrorReturnItem(RatesModel.Error)

  private fun requestRates(currency: Currency): Single<Rates> =
      api.getLatestRates(currency.name)
          .subscribeOn(Schedulers.io())


  private fun mergeRates(rates: Iterable<Rates>): Set<Rate> = HashSet<Rate>().apply {
    val supportedCurrencyNames = supportedCurrencies.map { it.name }
    rates.forEach {
      Currency.valueOf(it.base).let { source ->
        it.rates.filter { supportedCurrencyNames.contains(it.key) }.forEach { rate ->
          add(Rate(source = source, target = Currency.valueOf(rate.key), rate = rate.value))
        }
      }
    }
    supportedCurrencies.forEach {
      add(Rate(source = it, target = it, rate = 1.0))
    }
  }
}