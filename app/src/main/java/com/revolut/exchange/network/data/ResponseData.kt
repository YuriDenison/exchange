package com.revolut.exchange.network.data

import java.util.Date

object ResponseData {
  data class Rates(
      val base: String,
      val date: Date,
      val rates: Map<String, Double>
  )
}