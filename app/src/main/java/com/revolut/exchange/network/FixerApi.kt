package com.revolut.exchange.network

import com.revolut.exchange.network.data.ResponseData.Rates
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface FixerApi {
  @GET("/latest")
  fun getLatestRates(
      @Query("base") currency: String
  ): Single<Rates>
}