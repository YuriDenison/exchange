package com.revolut.exchange.repository

import android.app.Application
import android.net.NetworkInfo
import com.github.pwittchen.reactivenetwork.library.rx2.ConnectivityPredicate
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.jakewharton.rxrelay2.BehaviorRelay
import com.revolut.exchange.model.RatesModel
import com.revolut.exchange.network.FixerApiService
import com.revolut.exchange.util.CompositeDisposableComponent
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.concurrent.TimeUnit.SECONDS

open class RatesRepository(
    private val application: Application,
    private val api: FixerApiService) : CompositeDisposableComponent by CompositeDisposableComponent.Impl() {

  private val ratesStream = BehaviorRelay.create<RatesModel>()

  init {
    Observable.merge(timerObservable(), networkObservable())
        .flatMapSingle { api.latestRates() }
        .observeOn(Schedulers.computation())
        .subscribeOn(Schedulers.computation())
        .doOnNext { it.log() }
        .subscribe({ ratesStream.accept(it) }, Timber::e)
        .keepUntilDetach()
  }

  open fun observeRates(): Observable<RatesModel> = ratesStream

  private fun timerObservable() = Observable.interval(API_INTERVAL, SECONDS, Schedulers.io()).map { Unit }

  private fun networkObservable() = ReactiveNetwork.observeNetworkConnectivity(application)
      .filter(ConnectivityPredicate.hasState(NetworkInfo.State.CONNECTED))
      .map { Unit }

  private fun RatesModel.log() = when (this) {
    is RatesModel.Error -> Timber.d("Rates update: Error")
    is RatesModel.Data  -> Timber.d("Rates update: $data")
  }

  private companion object {
    const val API_INTERVAL = 30L // seconds
  }
}
