package com.revolut.exchange.dependency

import android.app.Application
import android.content.res.Resources
import com.revolut.exchange.dependency.module.AppModule
import com.revolut.exchange.dependency.module.NetworkModule
import com.revolut.exchange.dependency.module.RepositoryModule
import com.revolut.exchange.model.Currency
import com.revolut.exchange.preferences.AppPreferences
import com.revolut.exchange.repository.RatesRepository
import com.revolut.exchange.ui.navigation.AppRouter
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkModule::class, RepositoryModule::class))
interface AppComponent {
  fun application(): Application
  fun resources(): Resources
  fun preferences(): AppPreferences
  fun router(): AppRouter
  fun currencies(): Currency.Container

  fun ratesRepository(): RatesRepository
}