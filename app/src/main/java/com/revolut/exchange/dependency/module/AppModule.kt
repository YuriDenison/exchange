package com.revolut.exchange.dependency.module

import android.app.Application
import android.content.res.Resources
import com.revolut.exchange.ExchangeApplication
import com.revolut.exchange.model.Currency
import com.revolut.exchange.preferences.AppPreferences
import com.revolut.exchange.preferences.AppPreferencesImpl
import com.revolut.exchange.ui.navigation.AppRouter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class AppModule(private val app: ExchangeApplication) {
  @Provides
  @Singleton
  fun provideApp(): Application = app

  @Provides
  @Singleton
  fun provideResources(): Resources = app.resources

  @Provides
  @Singleton
  open fun provideAppPreferences(app: Application): AppPreferences = AppPreferencesImpl(app)

  @Provides
  @Singleton
  fun provideAppRouter(): AppRouter = AppRouter()

  @Provides
  @Singleton
  fun provideSupportedCurrencies(): Currency.Container = Currency.Container(Currency.values().toList())
}