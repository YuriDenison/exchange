package com.revolut.exchange.dependency.module

import android.app.Application
import com.revolut.exchange.network.FixerApiService
import com.revolut.exchange.repository.RatesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class RepositoryModule {
  @Provides
  @Singleton
  open fun provideRatesRepository(app: Application, api: FixerApiService) = RatesRepository(app, api)
}