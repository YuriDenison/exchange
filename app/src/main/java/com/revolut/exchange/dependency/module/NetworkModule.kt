package com.revolut.exchange.dependency.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.revolut.exchange.BuildConfig
import com.revolut.exchange.model.Currency
import com.revolut.exchange.network.FixerApi
import com.revolut.exchange.network.FixerApiService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
open class NetworkModule {
  @Provides
  @Singleton
  fun provideOkHttpClient(): OkHttpClient = OkHttpClient.Builder()
      .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
      .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
      .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
      .apply { if (BuildConfig.DEBUG) addInterceptor(HttpLoggingInterceptor { message -> Timber.tag("OkHttp").d(message) }.setLevel(Level.BODY)) }
      .build()

  @Provides
  @Singleton
  fun provideGson(): Gson = GsonBuilder()
      .setDateFormat("yyyy-MM-dd")
      .create()

  @Provides
  @Singleton
  fun provideConverterFactory(gson: Gson): Converter.Factory = GsonConverterFactory.create(gson)

  @Provides
  @Singleton
  fun provideRetrofit(client: OkHttpClient, converter: Converter.Factory): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BuildConfig.FIXER_URL)
        .client(client)
        .addConverterFactory(converter)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
  }

  @Provides
  @Singleton
  fun provideFixerApi(retrofit: Retrofit): FixerApi = retrofit.create(FixerApi::class.java)

  @Provides
  @Singleton
  open fun provideFixerApiService(api: FixerApi, currencies: Currency.Container): FixerApiService = FixerApiService(api, currencies.items)

  private companion object {
    const val CONNECT_TIMEOUT = 15L
    const val WRITE_TIMEOUT = 60L
    const val READ_TIMEOUT = 15L
  }
}