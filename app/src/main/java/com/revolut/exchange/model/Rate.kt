package com.revolut.exchange.model

data class Rate(val source: Currency, val target: Currency, val rate: Double) {
  override fun toString(): String = "($source -> $target: $rate)"
}