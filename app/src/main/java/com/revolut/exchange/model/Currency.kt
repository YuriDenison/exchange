package com.revolut.exchange.model

enum class Currency {
  USD, EUR, GBP;

  // Used for dagger
  data class Container(val items: List<Currency>)
}