package com.revolut.exchange.model

sealed class RatesModel {
  object Error : RatesModel()
  data class Data(val data: Set<Rate>) : RatesModel()
}