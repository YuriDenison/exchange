package com.revolut.exchange.model

data class Account(val portfolio: Map<Currency, Double>)