package com.revolut.exchange.model

data class Transaction(val base: Currency, val target: Currency, val amount: Double)