package com.revolut.exchange.util

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

interface CompositeDisposableComponent {
  var composite: CompositeDisposable

  fun Disposable.keepUntilDetach()

  fun resetCompositeDisposable() {
    synchronized(this) {
      composite.clear()
      composite = CompositeDisposable()
    }
  }

  class Impl : CompositeDisposableComponent {
    override var composite = CompositeDisposable()

    override fun Disposable.keepUntilDetach() {
      composite.add(this)
    }
  }
}