package com.revolut.exchange.util

import android.content.Context
import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.LayoutRes
import android.support.v4.content.ContextCompat
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.util.TypedValue.applyDimension
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit.MILLISECONDS

fun View.showKeyboard() = (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
fun View.hideKeyboard() = (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(windowToken, 0)

fun View.singleClicks(): Observable<Unit> = clicks().throttleFirst(300, MILLISECONDS, AndroidSchedulers.mainThread())

inline fun <reified T : View> ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): T {
  return context.layoutInflater.inflate(layoutRes, this, attachToRoot) as T
}

inline val Context.layoutInflater: LayoutInflater
  get() = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

inline fun TypedArray.use(crossinline block: (TypedArray) -> Unit) {
  try {
    block(this)
  } finally {
    recycle()
  }
}

fun View.getColor(@ColorRes colorResId: Int) = ContextCompat.getColor(context, colorResId)

fun View.getDrawable(@DrawableRes drawableResId: Int): Drawable = ContextCompat.getDrawable(context, drawableResId)

fun View.show() = apply { visibility = View.VISIBLE }
fun View.gone() = apply { visibility = View.GONE }
fun View.invisible() = apply { visibility = View.INVISIBLE }

fun View.animateToShow() = apply {
  animate().alpha(1f)
      .setDuration(VIEW_ANIMATION_DURATION)
      .withStartAction { alpha = 0f; visibility = View.VISIBLE }
      .start()
}

fun View.animateToGone() = apply {
  animate().alpha(0f)
      .setDuration(VIEW_ANIMATION_DURATION)
      .withEndAction { visibility = View.GONE }
      .start()
}

fun View.animateToInvisible() = apply {
  animate().alpha(0f)
      .setDuration(VIEW_ANIMATION_DURATION)
      .withEndAction { visibility = View.INVISIBLE }
      .start()
}

fun CharSequence.doubleValue(): Double? = toString().takeIf { it.isNotEmpty() }?.replace(',', '.')?.toDoubleOrNull()

var View.visible: Boolean
  get() = visibility == View.VISIBLE
  set(value) {
    visibility = if (value) View.VISIBLE else View.GONE
  }

inline fun <T : View> T.afterMeasured(crossinline f: T.() -> Unit) {
  viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
    override fun onGlobalLayout() {
      if (measuredWidth > 0 && measuredHeight > 0) {
        viewTreeObserver.removeOnGlobalLayoutListener(this)
        f()
      }
    }
  })
}

inline fun <T : ViewGroup> T.ifEmpty(block: T.() -> Unit): T {
  if (childCount == 0) block(this); return this
}

fun Context.dpToPx(dpValue: Int) = applyDimension(COMPLEX_UNIT_DIP, dpValue.toFloat(), resources.displayMetrics).toInt()

private const val VIEW_ANIMATION_DURATION = 200L