package com.revolut.exchange.util

import io.reactivex.Single

inline fun <reified T, R> List<Single<T>>.zipWith(crossinline zipper: (Iterable<T>) -> R): Single<R>
    = Single.zip(this, { zipper(it.filter { it is T }.map { it as T }) })
