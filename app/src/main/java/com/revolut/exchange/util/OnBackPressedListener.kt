package com.revolut.exchange.util

interface OnBackPressedListener {
  fun onBackPressed() = false
}