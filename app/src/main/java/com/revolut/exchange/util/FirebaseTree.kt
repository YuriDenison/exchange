package com.revolut.exchange.util

import android.util.Log
import com.google.firebase.crash.FirebaseCrash
import timber.log.Timber.Tree

class FirebaseTree : Tree() {
  override fun isLoggable(tag: String?, priority: Int) = priority >= Log.INFO

  override fun log(priority: Int, tag: String?, message: String?, t: Throwable?) {
    FirebaseCrash.log("${prioritySymbol(priority)}/$tag: $message")
    if (t != null && priority == Log.ERROR) {
      FirebaseCrash.report(t)
    }
  }

  private fun prioritySymbol(priority: Int) = when (priority) {
    Log.ERROR   -> "E"
    Log.WARN    -> "W"
    Log.INFO    -> "I"
    Log.DEBUG   -> "D"
    Log.VERBOSE -> "V"
    else        -> priority.toString()
  }
}

