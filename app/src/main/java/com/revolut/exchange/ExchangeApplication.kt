package com.revolut.exchange

import android.app.Application
import android.content.Context
import com.revolut.exchange.dependency.AppComponent
import com.revolut.exchange.dependency.DaggerAppComponent
import com.revolut.exchange.dependency.module.AppModule
import com.revolut.exchange.util.FirebaseTree
import io.reactivex.plugins.RxJavaPlugins
import timber.log.Timber

open class ExchangeApplication : Application() {
  private lateinit var component: AppComponent

  override fun onCreate() {
    super.onCreate()

    component = createAppComponent()

    initTimber()
    initRxJava()
    initRatesRepository()
  }

  protected open fun createAppComponent(): AppComponent =
      DaggerAppComponent.builder()
          .appModule(AppModule(this))
          .build()

  private fun initTimber() {
    Timber.plant(FirebaseTree())

    if (BuildConfig.DEBUG)
      Timber.plant(Timber.DebugTree())
  }

  private fun initRxJava() {
    RxJavaPlugins.setErrorHandler(Timber::e)
  }

  private fun initRatesRepository() {
    component.ratesRepository()
  }

  companion object {
    fun from(context: Context) = context.applicationContext as ExchangeApplication

    fun component(context: Context): AppComponent = (context.applicationContext as ExchangeApplication).component
  }
}