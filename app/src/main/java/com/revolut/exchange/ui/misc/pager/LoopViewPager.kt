package com.revolut.exchange.ui.misc.pager

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.util.AttributeSet


class LoopViewPager(context: Context, attrs: AttributeSet? = null) : ViewPager(context, attrs) {
  private lateinit var adapter: LoopPagerAdapterWrapper

  private val onPageChangeListeners: MutableList<OnPageChangeListener> = mutableListOf()
  private val onPageChangeListener = object : OnPageChangeListener {
    private var previousPosition = -1
    private var previousOffset = -1f

    override fun onPageSelected(position: Int) {
      val realPosition = adapter.toRealPosition(position)
      if (previousPosition != realPosition) {
        previousPosition = realPosition
        onPageChangeListeners.forEach { it.onPageSelected(realPosition) }
      }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
      val realPosition = adapter.toRealPosition(position)
      if (positionOffset == 0f && previousOffset == 0f && (position == 0 || position == adapter.count - 1)) {
        setCurrentItem(realPosition, false)
      }

      previousOffset = positionOffset
      onPageChangeListeners.forEach { listener ->
        when {
          realPosition != adapter.realCount - 1 -> listener.onPageScrolled(realPosition, positionOffset, positionOffsetPixels)
          positionOffset > .5                   -> listener.onPageScrolled(0, 0f, 0)
          else                                  -> listener.onPageScrolled(realPosition, 0f, 0)
        }
      }
    }

    override fun onPageScrollStateChanged(state: Int) {
      val position = super@LoopViewPager.getCurrentItem()
      val realPosition = adapter.toRealPosition(position)
      if (state == ViewPager.SCROLL_STATE_IDLE && (position == 0 || position == adapter.count - 1)) {
        setCurrentItem(realPosition, false)
      }

      onPageChangeListeners.forEach { it.onPageScrollStateChanged(state) }
    }
  }

  init {
    super.removeOnPageChangeListener(onPageChangeListener)
    super.addOnPageChangeListener(onPageChangeListener)
  }


  override fun setAdapter(adapter: PagerAdapter) {
    this.adapter = LoopPagerAdapterWrapper(adapter)
    super.setAdapter(this.adapter)
    setCurrentItem(0, false)
  }

  override fun getAdapter(): PagerAdapter = adapter.realAdapter

  override fun getCurrentItem(): Int = adapter.toRealPosition(super.getCurrentItem())

  override fun setCurrentItem(item: Int, smoothScroll: Boolean) {
    val innerItem = adapter.toInnerPosition(item)
    super.setCurrentItem(innerItem, smoothScroll)
  }

  override fun setCurrentItem(item: Int) {
    val innerItem = adapter.toInnerPosition(item)
    setCurrentItem(innerItem, true)
  }

  override fun setOnPageChangeListener(listener: OnPageChangeListener) {
    addOnPageChangeListener(listener)
  }

  override fun addOnPageChangeListener(listener: OnPageChangeListener) {
    onPageChangeListeners.add(listener)
  }

  override fun removeOnPageChangeListener(listener: OnPageChangeListener) {
    onPageChangeListeners.remove(listener)
  }

  override fun clearOnPageChangeListeners() {
    onPageChangeListeners.clear()
  }
}