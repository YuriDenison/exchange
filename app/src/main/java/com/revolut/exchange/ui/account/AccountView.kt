package com.revolut.exchange.ui.account

import com.revolut.exchange.model.Account
import com.revolut.exchange.model.Currency
import com.revolut.exchange.mvp.MvpView
import io.reactivex.Observable

interface AccountView : MvpView {
  fun bindTo(account: Account)
  fun showWelcomeMessage()

  fun observeViewActions(): Observable<ViewAction>

  sealed class ViewAction {
    object ExchangeClicked : ViewAction()
  }

}