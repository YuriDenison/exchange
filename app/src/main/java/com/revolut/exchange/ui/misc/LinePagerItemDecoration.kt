package com.revolut.exchange.ui.misc

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import com.revolut.exchange.R
import com.revolut.exchange.util.dpToPx


class LinePagerItemDecoration(context: Context) : RecyclerView.ItemDecoration() {
  private val colorActive = ContextCompat.getColor(context, android.R.color.white)
  private val colorInactive = ContextCompat.getColor(context, R.color.white__66)
  private val indicatorHeight = context.dpToPx(24)
  private val indicatorItemLength = context.dpToPx(16)
  private val indicatorItemPadding = context.dpToPx(8)
  private val indicatorStrokeWidth = context.dpToPx(2).toFloat()
  private val interpolator = AccelerateDecelerateInterpolator()

  private val mPaint = Paint().apply {
    strokeCap = Paint.Cap.ROUND
    strokeWidth = indicatorStrokeWidth
    style = Paint.Style.STROKE
    isAntiAlias = true
  }

  override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
    super.onDrawOver(c, parent, state)

    val itemCount = parent.adapter.itemCount

    // center horizontally, calculate width and subtract half from center
    val totalLength = indicatorItemLength * itemCount
    val paddingBetweenItems = Math.max(0, itemCount - 1) * indicatorItemPadding
    val indicatorTotalWidth = totalLength + paddingBetweenItems
    val indicatorStartX = (parent.width - indicatorTotalWidth) / 2f

    // center vertically in the allotted space
    val indicatorPosY = parent.height - indicatorHeight / 2f

    drawInactiveIndicators(c, indicatorStartX, indicatorPosY, itemCount)


    // find active page (which should be highlighted)
    val layoutManager = parent.layoutManager as LinearLayoutManager
    val activePosition = layoutManager.findFirstVisibleItemPosition()
    if (activePosition == RecyclerView.NO_POSITION) {
      return
    }

    // find offset of active page (if the user is scrolling)
    val activeChild = layoutManager.findViewByPosition(activePosition)
    val left = activeChild.left
    val width = activeChild.width

    // on swipe the active item will be positioned from [-width, 0]
    // interpolate offset for smooth animation
    val progress = interpolator.getInterpolation(left * -1 / width.toFloat())

    drawHighlights(c, indicatorStartX, indicatorPosY, activePosition, progress, itemCount)
  }

  private fun drawInactiveIndicators(c: Canvas, indicatorStartX: Float, indicatorPosY: Float, itemCount: Int) {
    mPaint.color = colorInactive
    val itemWidth = indicatorItemLength + indicatorItemPadding
    var start = indicatorStartX
    for (i in 0 until itemCount) {
      c.drawLine(start, indicatorPosY, start + indicatorItemLength, indicatorPosY, mPaint)
      start += itemWidth
    }
  }

  private fun drawHighlights(c: Canvas, indicatorStartX: Float, indicatorPosY: Float,
      highlightPosition: Int, progress: Float, itemCount: Int) {
    mPaint.color = colorActive

    val itemWidth = indicatorItemLength + indicatorItemPadding

    if (progress == 0f) {
      // no swipe, draw a normal indicator
      val highlightStart = indicatorStartX + itemWidth * highlightPosition
      c.drawLine(highlightStart, indicatorPosY,
          highlightStart + indicatorItemLength, indicatorPosY, mPaint)
    } else {
      var highlightStart = indicatorStartX + itemWidth * highlightPosition
      // calculate partial highlight
      val partialLength = indicatorItemLength * progress

      // draw the cut off highlight
      c.drawLine(highlightStart + partialLength, indicatorPosY,
          highlightStart + indicatorItemLength, indicatorPosY, mPaint)

      // draw the highlight overlapping to the next item as well
      if (highlightPosition < itemCount - 1) {
        highlightStart += itemWidth
        c.drawLine(highlightStart, indicatorPosY,
            highlightStart + partialLength, indicatorPosY, mPaint)
      }
    }
  }

  override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
    super.getItemOffsets(outRect, view, parent, state)
    outRect.bottom = indicatorHeight
  }
}