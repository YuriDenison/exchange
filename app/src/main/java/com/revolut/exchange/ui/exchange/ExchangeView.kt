package com.revolut.exchange.ui.exchange

import com.revolut.exchange.mvp.MvpView
import com.revolut.exchange.ui.exchange.currency.CurrencyItem
import io.reactivex.Observable

interface ExchangeView : MvpView {
  fun bindTo(model: ExchangeViewModel)

  fun observeViewActions(): Observable<ViewAction>

  data class ExchangeViewModel(
      val source: List<CurrencyItem.Model>,
      val target: List<CurrencyItem.Model>,
      val ratesError: Boolean,
      val transactionEnabled: Boolean
  )

  sealed class ViewAction {
    data class CurrencyUpdateAction(val model: CurrencyItem.Model) : ViewAction()
    object SubmitClicked : ViewAction()
  }
}