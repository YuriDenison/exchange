package com.revolut.exchange.ui.exchange.currency

import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Observable

class CurrencyAdapter(var data: List<CurrencyItem.Model> = emptyList()) : PagerAdapter() {
  private val actionStream = PublishRelay.create<CurrencyItem.ModelChanged>()

  override fun instantiateItem(container: ViewGroup, position: Int) = CurrencyItemView.create(container, actionStream).apply {
    tag = data[position].currency
    bindTo(data[position])
    container.addView(this)
  }

  fun observeViewActions(): Observable<CurrencyItem.ModelChanged> = actionStream

  fun getSelectedItem(position: Int) = data[position].copy(isFocused = data.any { it.isFocused })

  override fun destroyItem(container: ViewGroup, position: Int, obj: Any) {
    container.removeView(obj as View)
  }

  override fun isViewFromObject(view: View?, obj: Any?): Boolean = view == obj
  override fun getCount(): Int = data.size
}