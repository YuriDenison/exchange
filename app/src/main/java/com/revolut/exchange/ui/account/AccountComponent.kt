package com.revolut.exchange.ui.account

import com.revolut.exchange.dependency.AppComponent
import com.revolut.exchange.dependency.ScreenScope
import dagger.Component

@ScreenScope
@Component(dependencies = arrayOf(AppComponent::class))
interface AccountComponent {
  fun presenter(): AccountPresenter
}