package com.revolut.exchange.ui.navigation

import android.app.Activity
import android.content.Intent
import com.revolut.exchange.ui.exchange.ExchangeActivity

class AppRouter {
  private var activity: Activity? = null
  private var connectionsToScreens = 0

  fun openExchangeScreen() {
    activity?.let {
      it.startActivity(Intent(it, ExchangeActivity::class.java))
    }
  }

  fun closeScreen() {
    activity?.finish()
  }

  fun onActivityDestroyed() {
    if (--connectionsToScreens == 0) activity = null
  }

  fun onActivityCreated(activity: Activity?) {
    this.activity = activity
    connectionsToScreens++
  }
}