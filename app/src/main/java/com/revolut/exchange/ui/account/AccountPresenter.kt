package com.revolut.exchange.ui.account

import com.revolut.exchange.dependency.ScreenScope
import com.revolut.exchange.mvp.BasePresenter
import com.revolut.exchange.preferences.AppPreferences
import com.revolut.exchange.ui.account.AccountView.ViewAction
import com.revolut.exchange.ui.navigation.AppRouter
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber
import javax.inject.Inject

@ScreenScope
class AccountPresenter @Inject constructor(
    private val appPreferences: AppPreferences,
    private val router: AppRouter
) : BasePresenter<AccountView>() {

  override fun attachView(view: AccountView) {
    super.attachView(view)

    appPreferences.accountValue().asObservable()
        .startWith(appPreferences.accountValue().get())
        .subscribeUntilDetach(onNext = view::bindTo, onError = Timber::e)

    view.observeViewActions()
        .subscribeUntilDetach(
            onNext = this::handleViewAction,
            onError = Timber::e,
            scheduler = AndroidSchedulers.mainThread()
        )

    appPreferences.welcomeMessageShown().checkFlagWithAction { view.showWelcomeMessage() }
  }


  private fun handleViewAction(action: ViewAction) = when (action) {
    ViewAction.ExchangeClicked -> router.openExchangeScreen()
  }
}