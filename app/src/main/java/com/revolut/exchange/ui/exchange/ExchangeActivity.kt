package com.revolut.exchange.ui.exchange

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v4.view.ViewPager.OnPageChangeListener
import android.view.Menu
import android.view.MenuItem
import com.jakewharton.rxrelay2.PublishRelay
import com.revolut.exchange.R
import com.revolut.exchange.mvp.view.MvpActivity
import com.revolut.exchange.ui.exchange.ExchangeActivity.Snapshot
import com.revolut.exchange.ui.exchange.ExchangeView.ExchangeViewModel
import com.revolut.exchange.ui.exchange.ExchangeView.ViewAction
import com.revolut.exchange.ui.exchange.currency.CurrencyAdapter
import com.revolut.exchange.ui.exchange.currency.CurrencyItem.Model
import com.revolut.exchange.ui.exchange.currency.CurrencyItemView
import com.revolut.exchange.util.visible
import io.reactivex.Observable
import kotlinx.android.synthetic.main.ac_exchange.error
import kotlinx.android.synthetic.main.ac_exchange.pagerSource
import kotlinx.android.synthetic.main.ac_exchange.pagerTarget
import kotlinx.android.synthetic.main.ac_exchange.sourcePagerIndicator
import kotlinx.android.synthetic.main.ac_exchange.targetPagerIndicator
import kotlinx.android.synthetic.main.ac_exchange.toolbar
import me.relex.circleindicator.CircleIndicator

class ExchangeActivity : MvpActivity<ExchangeView, ExchangePresenter, Snapshot>(), ExchangeView {
  private lateinit var component: ExchangeComponent
  private lateinit var adapterSource: CurrencyAdapter
  private lateinit var adapterTarget: CurrencyAdapter

  private val viewActionsStream = PublishRelay.create<ViewAction>()

  private var transactionEnabled: Boolean = false
  private var scrollDetectionEnabled: Boolean = false
  private var initialBind: Boolean = true

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setSupportActionBar(toolbar)

    toolbar.setNavigationOnClickListener { onBackPressed() }
    supportActionBar?.let {
      it.setDisplayHomeAsUpEnabled(true)
      it.setDisplayShowHomeEnabled(true)
      it.setDisplayShowTitleEnabled(false)
    }
  }

  private fun setUpViewPager(pager: ViewPager, adapter: CurrencyAdapter, indicator: CircleIndicator) {
    pager.adapter = adapter
    pager.addOnPageChangeListener(object : OnPageChangeListener {
      override fun onPageSelected(position: Int) {
        viewActionsStream.accept(ViewAction.CurrencyUpdateAction(adapter.getSelectedItem(position)))
      }

      override fun onPageScrollStateChanged(state: Int) {}
      override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
    })
    indicator.setViewPager(pager)
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menu.add(0, R.id.menu_item_ok, 0, R.string.ok).apply {
      setIcon(R.drawable.sl_check)
      setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
    }
    return true
  }

  override fun onPrepareOptionsMenu(menu: Menu): Boolean {
    menu.findItem(R.id.menu_item_ok).isEnabled = transactionEnabled
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.menu_item_ok -> viewActionsStream.accept(ViewAction.SubmitClicked)
    }
    return true
  }

  override fun bindTo(model: ExchangeViewModel) {
    scrollDetectionEnabled = false

    if (initialBind) {
      initialBind = false

      setUpViewPager(pagerSource, adapterSource.apply { data = model.source }, sourcePagerIndicator)
      setUpViewPager(pagerTarget, adapterTarget.apply { data = model.target }, targetPagerIndicator)
    }

    adapterSource.data = model.source
    adapterTarget.data = model.target

    updateViewPager(pagerSource, model.source)
    updateViewPager(pagerTarget, model.target)

    error.visible = model.ratesError
    transactionEnabled = model.transactionEnabled
    invalidateOptionsMenu()


    scrollDetectionEnabled = true
  }

  private fun updateViewPager(pager: ViewPager, data: List<Model>) {
    data.forEach {
      pager.findViewWithTag<CurrencyItemView>(it.currency)?.bindTo(it)
    }
  }

  override fun observeViewActions(): Observable<ViewAction> = Observable.merge(viewActionsStream, adapterActions())

  private fun adapterActions(): Observable<ViewAction> = Observable.merge(
      adapterSource.observeViewActions().map { ViewAction.CurrencyUpdateAction(it.model) },
      adapterTarget.observeViewActions().map { ViewAction.CurrencyUpdateAction(it.model) }
  )

  override fun injectDependencies() {
    component = DaggerExchangeComponent.builder()
        .appComponent(appComponent())
        .build()
    adapterSource = CurrencyAdapter()
    adapterTarget = CurrencyAdapter()
  }

  override fun getLayoutId() = R.layout.ac_exchange

  override fun createPresenter() = component.presenter()

  override fun takeSnapshot() = Snapshot(component, adapterSource, adapterTarget)

  override fun restoreFromSnapshot(snapshot: Snapshot) {
    this.component = snapshot.component
    this.adapterSource = snapshot.adapterBase
    this.adapterTarget = snapshot.adapterTarget
  }

  data class Snapshot(val component: ExchangeComponent, val adapterBase: CurrencyAdapter, val adapterTarget: CurrencyAdapter)
}
