package com.revolut.exchange.ui.misc

interface ItemView<in T> {
  fun bindTo(model: T): Boolean
}