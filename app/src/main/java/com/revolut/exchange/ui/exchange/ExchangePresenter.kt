package com.revolut.exchange.ui.exchange

import com.jakewharton.rxrelay2.BehaviorRelay
import com.revolut.exchange.dependency.ScreenScope
import com.revolut.exchange.model.Account
import com.revolut.exchange.model.Currency
import com.revolut.exchange.model.Rate
import com.revolut.exchange.model.RatesModel
import com.revolut.exchange.mvp.BasePresenter
import com.revolut.exchange.preferences.AppPreferences
import com.revolut.exchange.repository.RatesRepository
import com.revolut.exchange.ui.exchange.ExchangeView.ExchangeViewModel
import com.revolut.exchange.ui.exchange.ExchangeView.ViewAction
import com.revolut.exchange.ui.exchange.currency.CurrencyItem
import com.revolut.exchange.ui.exchange.currency.CurrencyItem.Side
import com.revolut.exchange.ui.exchange.currency.CurrencyItem.Side.SOURCE
import com.revolut.exchange.ui.exchange.currency.CurrencyItem.Side.TARGET
import com.revolut.exchange.ui.navigation.AppRouter
import io.reactivex.rxkotlin.Observables
import timber.log.Timber
import javax.inject.Inject

@ScreenScope
class ExchangePresenter @Inject constructor(
    private val ratesRepository: RatesRepository,
    private val appPreferences: AppPreferences,
    private val appRouter: AppRouter,
    private val supportedCurrencies: Currency.Container
) : BasePresenter<ExchangeView>() {
  private var sourceData: CurrencyItem.Model = CurrencyItem.Model(
      side = SOURCE,
      currency = supportedCurrencies.items[0],
      portfolioAmount = appPreferences.accountValue().get().portfolio.getOrDefault(supportedCurrencies.items[0], 0.0),
      amount = 0.0,
      isFocused = true,
      isValid = true
  )

  private var targetData: CurrencyItem.Model = CurrencyItem.Model(
      side = TARGET,
      currency = supportedCurrencies.items[0],
      portfolioAmount = appPreferences.accountValue().get().portfolio.getOrDefault(supportedCurrencies.items[0], 0.0),
      amount = 0.0,
      isFocused = false,
      isValid = true
  )

  private val currencyStream = BehaviorRelay.create<CurrencyItem.Model>().apply { accept(sourceData) }

  override fun attachView(view: ExchangeView) {
    super.attachView(view)

    currencyStream.filter { it.isFocused.not() }
        .subscribeUntilDetach(onNext = {
          when (it.side) {
            SOURCE -> sourceData = it
            TARGET -> targetData = it
          }

          (sourceData.takeIf { it.isFocused } ?: targetData.takeIf { it.isFocused })?.let {
            currencyStream.accept(it)
          }
        })

    Observables.combineLatest(
        appPreferences.accountValue().asObservable().startWith(appPreferences.accountValue().get()),
        ratesRepository.observeRates().startWith(RatesModel.Error),
        currencyStream.filter { it.isFocused },
        { account, ratesModel, model -> prepareExchangeViewModel(account, ratesModel, model) })
        .subscribeUntilDetach(onNext = view::bindTo, onError = Timber::e)

    view.observeViewActions()
        .subscribeUntilDetach(onNext = this::handleViewAction, onError = Timber::e)
  }

  private fun prepareExchangeViewModel(account: Account, ratesModel: RatesModel, actionModel: CurrencyItem.Model): ExchangeViewModel {
    val rates = (ratesModel as? RatesModel.Data)?.data
    val focusedModel = actionModel.takeIf { it.isFocused } ?: sourceData.takeIf { it.isFocused } ?: targetData

    val sourceModels = supportedCurrencies.items.map { it.createItemModel(Side.SOURCE, rates, account.portfolio[it] ?: 0.0, focusedModel.side, focusedModel.amount, focusedModel.currency) }
    val targetModels = supportedCurrencies.items.map { it.createItemModel(Side.TARGET, rates, account.portfolio[it] ?: 0.0, focusedModel.side, focusedModel.amount, focusedModel.currency) }

    sourceModels.firstOrNull { it.isFocused }?.let { sourceData = it } ?:
        sourceModels.firstOrNull { it.currency == sourceData.currency }?.let { sourceData = sourceData.copy(amount = it.amount) }

    targetModels.firstOrNull { it.isFocused }?.let { targetData = it } ?:
        targetModels.firstOrNull { it.currency == targetData.currency }?.let { targetData = targetData.copy(amount = it.amount) }

    val transactionEnabled = (ratesModel !is RatesModel.Error) && sourceData.amount.abs() > 0 && account.portfolio[sourceData.currency]?.let { it > sourceData.amount.abs() } == true

    Timber.d(" ")
    Timber.d("---- Focused Item Changed ----")
    Timber.d("Focused Item : $focusedModel")
    Timber.d("Source : ${sourceModels.map { it.currency to it.isFocused }}")
    Timber.d("Target : ${targetModels.map { it.currency to it.isFocused }}")
    Timber.d("---- End Model ----")
    Timber.d(" ")
    return ExchangeViewModel(sourceModels, targetModels, ratesModel is RatesModel.Error, transactionEnabled)
  }

  private fun Currency.createItemModel(side: Side, rates: Set<Rate>?, accountAmount: Double, focusedSide: Side, focusedAmount: Double, focusedCurrency: Currency): CurrencyItem.Model {
    val amountMultiplier = if (side == SOURCE) -1 else 1
    val rate =
        if ((side == SOURCE && focusedSide == SOURCE) or (side == TARGET && focusedSide == TARGET)) 0.0
        else rates?.firstOrNull { it.source == focusedCurrency && it.target == this }?.rate ?: 0.0
    val amount =
        if (side == focusedSide && this == focusedCurrency) focusedAmount.abs() * amountMultiplier
        else if (side == focusedSide && this != focusedCurrency) 0.0
        else focusedAmount.abs() * rate * amountMultiplier
    val isFocused = side == focusedSide && this == focusedCurrency
    return CurrencyItem.Model(side, this, accountAmount, amount, isFocused, amount + accountAmount >= 0)
  }


  private fun handleViewAction(action: ViewAction) = when (action) {
    is ViewAction.CurrencyUpdateAction -> currencyStream.accept(action.model)
    is ViewAction.SubmitClicked        -> handleSubmitClicked()
  }

  private fun Double.abs() = Math.abs(this)

  private fun handleSubmitClicked() {
    appPreferences.accountValue().updateWith(
        sourceData.currency to sourceData.amount,
        targetData.currency to targetData.amount
    )
    appRouter.closeScreen()
  }
}