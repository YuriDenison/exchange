package com.revolut.exchange.ui.misc.pager

import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup

/*
*  Simple loop pager adapter without support for empty adapters
*/
class LoopPagerAdapterWrapper internal constructor(internal val realAdapter: PagerAdapter) : PagerAdapter() {
  internal val realCount: Int
    get() = realAdapter.count


  internal fun toRealPosition(position: Int): Int = when (position) {
    0             -> realCount - 1
    realCount + 1 -> 0
    else          -> position - 1
  }

  internal fun toInnerPosition(realPosition: Int) = realPosition + 1

  override fun getCount(): Int = realCount + 2

  override fun instantiateItem(container: ViewGroup, position: Int): Any =
      realAdapter.instantiateItem(container, toRealPosition(position))

  override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
    realAdapter.destroyItem(container, toRealPosition(position), `object`)
  }

  override fun finishUpdate(container: ViewGroup) {
    realAdapter.finishUpdate(container)
  }

  override fun isViewFromObject(view: View, `object`: Any) = realAdapter.isViewFromObject(view, `object`)

  override fun restoreState(bundle: Parcelable?, classLoader: ClassLoader?) {
    realAdapter.restoreState(bundle, classLoader)
  }

  override fun saveState(): Parcelable? = realAdapter.saveState()

  override fun startUpdate(container: ViewGroup) {
    realAdapter.startUpdate(container)
  }

  override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
    realAdapter.setPrimaryItem(container, toRealPosition(position), `object`)
  }
}