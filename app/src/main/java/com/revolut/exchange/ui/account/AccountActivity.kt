package com.revolut.exchange.ui.account

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.PagerSnapHelper
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.OnScrollListener
import android.widget.LinearLayout.HORIZONTAL
import com.revolut.exchange.R
import com.revolut.exchange.model.Account
import com.revolut.exchange.mvp.view.MvpActivity
import com.revolut.exchange.ui.account.AccountActivity.Snapshot
import com.revolut.exchange.ui.account.AccountView.ViewAction
import com.revolut.exchange.ui.account.portfolio.PortfolioAdapter
import com.revolut.exchange.ui.account.portfolio.PortfolioItemModel
import com.revolut.exchange.ui.misc.LinePagerItemDecoration
import com.revolut.exchange.util.singleClicks
import com.revolut.exchange.util.visible
import io.reactivex.Observable
import kotlinx.android.synthetic.main.ac_account.content
import kotlinx.android.synthetic.main.ac_account.exchangeButton
import kotlinx.android.synthetic.main.ac_account.leftArrow
import kotlinx.android.synthetic.main.ac_account.portfolio
import kotlinx.android.synthetic.main.ac_account.rightArrow

class AccountActivity : MvpActivity<AccountView, AccountPresenter, Snapshot>(), AccountView {
  private lateinit var component: AccountComponent
  private lateinit var adapter: PortfolioAdapter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val layoutManager = LinearLayoutManager(this@AccountActivity, HORIZONTAL, false)
    portfolio.setHasFixedSize(false)
    portfolio.layoutManager = layoutManager
    portfolio.adapter = adapter
    portfolio.addItemDecoration(LinePagerItemDecoration(this))
    portfolio.addOnScrollListener(object : OnScrollListener() {
      override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val position = layoutManager.findLastCompletelyVisibleItemPosition()
        leftArrow.visible = position != 0
        rightArrow.visible = position < adapter.data.size - 1
        leftArrow.setOnClickListener { portfolio.smoothScrollToPosition(position - 1) }
        rightArrow.setOnClickListener { portfolio.smoothScrollToPosition(position + 1) }
      }
    })

    PagerSnapHelper().attachToRecyclerView(portfolio)
  }

  override fun observeViewActions(): Observable<ViewAction> = exchangeButton.singleClicks().map { ViewAction.ExchangeClicked }

  override fun bindTo(account: Account) = account.portfolio.entries.let {
    adapter.data = it.map { PortfolioItemModel(it.key, it.value) }
  }

  override fun showWelcomeMessage() {
    Snackbar.make(content, R.string.message_welcome, Snackbar.LENGTH_INDEFINITE)
        .setAction(R.string.got_it, {})
        .show()
  }

  override fun injectDependencies() {
    component = DaggerAccountComponent.builder()
        .appComponent(appComponent())
        .build()
    adapter = PortfolioAdapter()
  }

  override fun getLayoutId() = R.layout.ac_account

  override fun createPresenter() = component.presenter()

  override fun takeSnapshot() = Snapshot(component, adapter)

  override fun restoreFromSnapshot(snapshot: Snapshot) {
    this.component = snapshot.component
    this.adapter = snapshot.adapter
  }

  data class Snapshot(val component: AccountComponent, val adapter: PortfolioAdapter)
}
