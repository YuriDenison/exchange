package com.revolut.exchange.ui.exchange.currency

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.focusChanges
import com.jakewharton.rxbinding2.widget.textChanges
import com.jakewharton.rxrelay2.PublishRelay
import com.revolut.exchange.R
import com.revolut.exchange.model.Currency
import com.revolut.exchange.ui.exchange.currency.CurrencyItem.Model
import com.revolut.exchange.ui.exchange.currency.CurrencyItem.ModelChanged
import com.revolut.exchange.ui.misc.ItemView
import com.revolut.exchange.util.CompositeDisposableComponent
import com.revolut.exchange.util.doubleValue
import com.revolut.exchange.util.inflate
import com.revolut.exchange.util.showKeyboard
import kotlinx.android.synthetic.main.item_currency.view.amount
import kotlinx.android.synthetic.main.item_currency.view.currencyAccount
import kotlinx.android.synthetic.main.item_currency.view.currencyName
import timber.log.Timber
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit.MILLISECONDS

class CurrencyItemView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs), ItemView<CurrencyItem.Model>,
    CompositeDisposableComponent by CompositeDisposableComponent.Impl() {
  private lateinit var actionsStream: PublishRelay<ModelChanged>

  private var ignoreUpdateFlag = false
  private var model: Model? = null

  override fun onFinishInflate() {
    super.onFinishInflate()

    if (isInEditMode)
      return

    amount.textChanges().skipInitialValue()
        .filter { ignoreUpdateFlag.not() && it.doubleValue() != null }
        .map { it.doubleValue() ?: 0.0 }
        .debounce(DELAY, MILLISECONDS)
        .subscribe({ model?.let { model -> actionsStream.accept(ModelChanged(model.copy(amount = it))) } }, Timber::e)
        .keepUntilDetach()

    amount.focusChanges()
        .filter { it && ignoreUpdateFlag.not() && (model?.isFocused ?: false).not() }
        .subscribe({ model?.let { model -> actionsStream.accept(ModelChanged(model.copy(isFocused = true))) } }, Timber::e)
        .keepUntilDetach()
  }

  override fun bindTo(model: Model) = post {
    val requestFocus = amount.hasFocus().not() && (this.model?.isFocused ?: false).not() && model.isFocused
    this.model = model

    ignoreUpdateFlag = true

    if (requestFocus) {
      amount.requestFocus()
      amount.showKeyboard()
    }

    val selection = amount.text.length - amount.selectionEnd
    val amountText = model.amount.format()
    amount.setText(amountText)
    (amountText.length - selection).takeIf { it >= 0 && it <= amountText.length }?.let {
      amount.setSelection(it)
    }

    currencyName.text = model.currency.name
    currencyAccount.text = context.getString(R.string.account_currency_amount, "${model.currency.sign()}${model.portfolioAmount.format()}")
    currencyAccount.isActivated = model.isValid.not()

    ignoreUpdateFlag = false
  }

  private fun Currency.sign() = context.getString(when (this) {
    Currency.GBP -> R.string.sign_gbp
    Currency.EUR -> R.string.sign_eur
    Currency.USD -> R.string.sign_usd
  })

  private fun Double.format() = DECIMAL_FORMAT.format(this)

  companion object {
    private const val DELAY = 100L
    private val DECIMAL_FORMAT = DecimalFormat("#.##")

    fun create(parent: ViewGroup, actionsStream: PublishRelay<ModelChanged>) = parent.inflate<CurrencyItemView>(R.layout.item_currency).apply {
      this.actionsStream = actionsStream
    }
  }
}