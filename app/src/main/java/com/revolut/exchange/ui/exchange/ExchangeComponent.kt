package com.revolut.exchange.ui.exchange

import com.revolut.exchange.dependency.AppComponent
import com.revolut.exchange.dependency.ScreenScope
import dagger.Component

@ScreenScope
@Component(dependencies = arrayOf(AppComponent::class))
interface ExchangeComponent {
  fun presenter(): ExchangePresenter
}