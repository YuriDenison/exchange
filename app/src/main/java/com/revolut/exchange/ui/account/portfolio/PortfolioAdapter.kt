package com.revolut.exchange.ui.account.portfolio

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.revolut.exchange.ui.misc.SimpleViewHolder

class PortfolioAdapter : RecyclerView.Adapter<SimpleViewHolder<PortfolioItemModel, PortfolioItemView>>() {
  var data: List<PortfolioItemModel> = emptyList()
    set(value) {
      field = value
      notifyDataSetChanged()
    }

  init {
    setHasStableIds(true)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SimpleViewHolder(PortfolioItemView.create(parent))

  override fun onBindViewHolder(holder: SimpleViewHolder<PortfolioItemModel, PortfolioItemView>, position: Int) {
    holder.bindTo(data[position])
  }

  override fun getItemId(position: Int) = data[position].currency.name.hashCode().toLong()

  override fun onFailedToRecycleView(holder: SimpleViewHolder<PortfolioItemModel, PortfolioItemView>) = true

  override fun getItemCount() = data.size
}