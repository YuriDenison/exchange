package com.revolut.exchange.ui.account.portfolio

import com.revolut.exchange.model.Currency

data class PortfolioItemModel(
    val currency: Currency,
    val amount: Double
)
