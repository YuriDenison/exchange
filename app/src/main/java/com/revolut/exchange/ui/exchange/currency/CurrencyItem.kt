package com.revolut.exchange.ui.exchange.currency

import com.revolut.exchange.model.Currency

object CurrencyItem {
  data class Model(
      val side: Side,
      val currency: Currency,
      val portfolioAmount: Double,
      val amount: Double,
      val isFocused: Boolean = false,
      val isValid: Boolean = false
  )

  data class ModelChanged(val model: CurrencyItem.Model)

  enum class Side { SOURCE, TARGET }
}