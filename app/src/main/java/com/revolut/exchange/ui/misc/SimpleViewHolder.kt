package com.revolut.exchange.ui.misc

import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View

open class SimpleViewHolder<in T : Any, out V>(val view: V) : ViewHolder(view) where V : View, V : ItemView<T> {
  fun bindTo(data: T) {
    view.bindTo(data)
  }
}