package com.revolut.exchange.ui.account.portfolio

import android.annotation.SuppressLint
import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.ViewGroup
import com.revolut.exchange.R
import com.revolut.exchange.model.Currency
import com.revolut.exchange.ui.misc.ItemView
import com.revolut.exchange.util.inflate
import kotlinx.android.synthetic.main.item_portfolio.view.currency
import kotlinx.android.synthetic.main.item_portfolio.view.value
import java.text.DecimalFormat

class PortfolioItemView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs), ItemView<PortfolioItemModel> {
  @SuppressLint("SetTextI18n")
  override fun bindTo(model: PortfolioItemModel) = post {
    value.text = "${model.currency.sign()}${model.amount.format()}"
    currency.text = "${model.currency.name} · ${model.currency.description()}"
  }

  private fun Currency.sign() = context.getString(when (this) {
    Currency.GBP -> R.string.sign_gbp
    Currency.EUR -> R.string.sign_eur
    Currency.USD -> R.string.sign_usd
  })

  private fun Currency.description() = context.getString(when (this) {
    Currency.GBP -> R.string.description_gbp
    Currency.EUR -> R.string.description_eur
    Currency.USD -> R.string.description_usd
  })

  private fun Double.format() = DECIMAL_FORMAT.format(this)

  companion object {
    private val DECIMAL_FORMAT = DecimalFormat("#.##")

    fun create(parent: ViewGroup) = parent.inflate<PortfolioItemView>(R.layout.item_portfolio)
  }
}